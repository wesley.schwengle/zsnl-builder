#!/usr/bin/env bash

function get_boxes {
    aws autoscaling describe-auto-scaling-groups --region eu-central-1 \
      | jq -r '.AutoScalingGroups[]
               | select(.AutoScalingGroupARN | contains("xxllnc-ci-arm")).Instances[]
               | select(.LifecycleState      | contains("InService")).InstanceId' \
      | xargs printf "xxllnc-ci-arm-%s.dmz.zsys.nl\n"
}

## Create a builder instance and select it
BUILDER=$( docker buildx create --use --platform linux/amd64 --name mintlab-ci )

## Loop over all boxes and add to builder instance
for box in $( get_boxes ); do
    echo "Adding box '$box' to builder '$BUILDER'"
    docker buildx create --append --driver docker-container --name "$BUILDER" --platform linux/arm64 --node "$box" "$box:2375"
done

## Bootstrap our cluster
echo "Bootstrapping buildx on all nodes"
docker buildx inspect --bootstrap
